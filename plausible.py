#!/usr/bin/python3

import sys
import os
import random
import string
import subprocess

# this class will be used for switching
class Switcher:
  # member variables
  categories = [ 
    # CPP Category
    {"files": [], "extensions": ["cpp", "c", "h", "hpp"]},
    
    # Text Category
    {"files": [], "extensions": ["txt", "log"]},
    
    # Markdown Category
    {"files": [], "extensions": ["md"]},
    
    # Python Category
    {"files": [], "extensions": ["py"]},
    
    # Markup Language Category
    {"files": [], "extensions": ["html", "xml", "htm"]}, 
    
    # Misc Category
    {"files": [], "extensions": []}
  ]
    
  def enroll_file(self, filename, ext):
    for c in self.categories:
      if ext in c['extensions']:
        c['files'].append(filename)
        return
    self.categories[-1]['files'].append(filename)
    
  def get_extension_category(self):
    cat = None
    ext = None
    
    try:
      cat = random.choice(self.categories)
      ext = random.choice(cat['extensions'])
    
    # chose misc category, no extensions
    except IndexError:
        ext = ""
        
    return (cat['files'], ext)

# global var
SWITCHER = Switcher()

# categorizes a file
def process(filename):
  # filter non ascii files
  p = subprocess.Popen('/usr/bin/file ' + filename,
                       stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE,
                       shell=True)

  output, errors = p.communicate()
  # filtering happens here
  if "ASCII" not in str(output):
    return

  # filename extension
  ext = "NONE"

  # get file extension
  filenamearray = filename.split('.')
  if len(filenamearray) < 2:
    # no file extension
    pass
    
  else:  
    # get last segment of file name
    ext = filenamearray[-1]
    
  # enroll file in switcher
  SWITCHER.enroll_file(filename, ext)

def traverse(root):
  # should contain all files
  collection = []

  # traverse file system starting with root
  walky_boy = os.walk(root)
  for directory, _, files in walky_boy:
    for f in files:
      collection.append("" + directory + "/" + f)

  # process results
  # map(process, collection)
  for i in collection:
    process(i)


if __name__ == '__main__':

  # check arg count
  if not len(sys.argv) == 2:
      print("Usage: ./plausible.py N")
      print("N is the number of files you wish to generate")
      sys.exit()

  # Traverse Home Dir
  print("[+] Traversing home dir")
  traverse(os.environ["HOME"])

  # generate random files according to arg
  for i in range(int(sys.argv[1])):
 
    # generate model args
    model, ext = SWITCHER.get_extension_category()

    args = ' '.join(model)

    # generate filename
    filename = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8)) + "." + ext
    print('[+] Writing File: ' + filename)
    filename = os.environ["HOME"] + "/plausible/" + filename

    # get body of file
    m = subprocess.Popen('rust_markov/target/release/rust_markov -w6 -l8000' + args,  stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE,
                       shell=True)
    output, errors = m.communicate()

    # write file data
    f = open(filename, 'w')
    f.write(str(output, "utf-8"))
    f.close()
