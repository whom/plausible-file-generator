use std::collections::HashMap;

use markov_model::*;

pub struct MarkovBuilderNode {
    next: HashMap<Id, u32>, // maps ids to their count
}

impl MarkovBuilderNode {
    fn new() -> MarkovBuilderNode {
        MarkovBuilderNode {
            next: HashMap::new(),
        }
    }

    fn insert(&mut self, next_id: Id) {
        let old_count: u32;
        match self.next.get(&next_id) {
            Some(count) => {
                old_count = *count;
            }
            None => {
                old_count = 0;
            }
        }
        self.next.insert(next_id, old_count + 1);
    }

    fn compile(&self) -> MarkovNode {
        let mut node = MarkovNode::new();
        for (id, count) in &self.next {
            node.push(*id, *count);
        }
        node
    }
}

pub struct MarkovBuilder {
    nodes: HashMap<Id, MarkovBuilderNode>,
    next_id: u32,
}

impl MarkovBuilder {
    pub fn new() -> MarkovBuilder {
        MarkovBuilder {
            nodes: HashMap::new(),
            next_id: 0,
        }
    }

    pub fn new_id(&mut self) -> u32 {
        let id = self.next_id;
        self.next_id += 1;
        id
    }

    fn insert_id(&mut self, target: Id, next: Id) {
        let mut node: MarkovBuilderNode = self
            .nodes
            .remove(&target)
            .unwrap_or(MarkovBuilderNode::new());
        node.insert(next);
        self.nodes.insert(target, node);
    }

    pub fn insert_ids(&mut self, ids: Vec<u32>) {
        let mut prev = Id::Start;
        for id in ids {
            self.insert_id(prev, Id::Id(id));
            prev = Id::Id(id);
        }
        self.insert_id(prev, Id::Start);
    }

    pub fn compile(&self) -> MarkovModel {
        let mut nodes: HashMap<Id, MarkovNode> = HashMap::new();
        for (id, node) in &self.nodes {
            nodes.insert(*id, node.compile());
        }
        MarkovModel::new(nodes)
    }

    pub fn to_string(&self) -> String {
        format!(
            "builder nodes: [\n{}]\n",
            self.nodes
                .iter()
                .fold(String::new(), |acc, (id, node)| {
                    format!(
                        "{}{}\n",
                        acc,
                        format!(
                            "  {}: {{\n{}  }}",
                            id,
                            node.next
                                .iter()
                                .fold(String::new(), |acc, (id, count)| {
                                    format!("{}    {}: {},\n", acc, id, count)
                                })
                        )
                    )
                })
        )
    }
}
