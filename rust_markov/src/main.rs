extern crate regex;

mod markov_builder;
mod markov_model;

use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::prelude::*;

use markov_builder::*;
use markov_model::*;

struct StringMarkovBuilder {
    model: MarkovBuilder,
    strings: HashMap<String, u32>,
    next_str_id: u32,
    values: HashMap<Vec<u32>, u32>,
    max_window_size: usize,
}

impl StringMarkovBuilder {
    fn new(max_window_size: usize) -> StringMarkovBuilder {
        StringMarkovBuilder {
            model: markov_builder::MarkovBuilder::new(),
            strings: HashMap::new(),
            next_str_id: 0,
            values: HashMap::new(),
            max_window_size: max_window_size,
        }
    }

    fn id_for_string(&mut self, string: String) -> u32 {
        let result = match self.strings.get(&string) {
            Some(id) => Some(*id),
            None => None,
        };
        match result {
            Some(id) => id,
            None => {
                let id = self.next_str_id;
                self.next_str_id += 1;
                self.strings.insert(string, id);
                id
            }
        }
    }

    fn id_for_seq(&mut self, seq: Vec<u32>) -> u32 {
        let result = match self.values.get(&seq) {
            Some(id) => Some(*id),
            None => None,
        };
        match result {
            Some(id) => id,
            None => {
                let id = self.model.new_id();
                self.values.insert(seq, id);
                id
            }
        }
    }

    fn insert_strings(&mut self, strings: Vec<String>) {
        let mut window = vec![];
        let mut seqs = vec![];
        for string in strings {
            window.push(self.id_for_string(string));
            while window.len() > self.max_window_size {
                window.drain(0..1);
            }
            seqs.push(window.clone());
        }
        let ids = seqs.iter().map(|i| self.id_for_seq(i.clone())).collect();
        self.model.insert_ids(ids);
    }

    fn compile(&self) -> StringMarkovModel {
        let mut strings = HashMap::new();
        for (id, string) in &self.strings {
            strings.insert(*string, id.clone());
        }
        let mut values = HashMap::new();
        for (id, value) in &self.values {
            values.insert(*value, id.clone());
        }
        StringMarkovModel {
            model: self.model.compile(),
            strings: strings,
            values: values,
        }
    }

    fn to_string(&self) -> String {
        self.model.to_string()
    }
}

struct StringMarkovModel {
    model: MarkovModel,
    strings: HashMap<u32, String>,
    values: HashMap<u32, Vec<u32>>,
}

impl StringMarkovModel {
    fn generate(&self, max_len: u32) -> Vec<String> {
        let ids = self.model.generate(max_len);
        let mut out = vec![];
        for id in ids {
            out.push(match self.values.get(&id) {
                Some(seq) => {
                    let id = seq.last().unwrap();
                    match self.strings.get(id) {
                        Some(s) => s.clone(),
                        None => {
                            eprintln!("id {} failed to map to string", id);
                            "[ERROR]".to_string()
                        }
                    }
                }
                None => {
                    eprintln!("id {} failed to map to seq", id);
                    "[ERROR]".to_string()
                }
            })
        }
        out
    }

    fn to_string(&self) -> String {
        self.model.to_string(&|id| match id {
            Id::Id(id) => match self.values.get(id) {
                Some(seq) => format!(
                    "[{}]",
                    seq.iter().fold(String::new(), |acc, id| format!(
                        "{}{}'{}'",
                        acc,
                        if acc.is_empty() { "" } else { ", " },
                        match self.strings.get(id) {
                            Some(token) => token
                                .replace("\\", "\\\\")
                                .replace("\n", "\\n")
                                .replace("'", "\'"),
                            None => "[INVALID]".to_string(),
                        }
                    ))
                ),
                None => "[INVALID]".to_string(),
            },
            Id::Start => "[START]".to_string(),
        })
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let re = regex::Regex::new(r"([\w]+)|([\s]+)|([^\w\s]+)").unwrap();

    let mut window: usize = 1;
    let mut limit: u32 = 1000;
    let mut debug = false;
    let mut files: Vec<String> = vec![];

    let mut first = true;
    for i in args {
        if first {
            first = false;
        } else {

            if i == "-h" || i == "--help" {
                println!("usage:");
                println!("    -h, --help        this help");
                println!("    -d                debug mode");
                println!("    -w<WINDOW>        set the window size (default is 1)");
                println!("    -l<LIMIT>         set the token limit (default is 1000)");
                println!();
                panic!("I'm too lazy to figure out how to quit properly");
            } else if i.starts_with("-w") {
                window = i[2..].parse::<usize>().unwrap();
            } else if i.starts_with("-l") {
                limit = i[2..].parse::<u32>().unwrap();
            } else if i == "-d" {
                debug = true;
            } else {
                files.push(i.clone());
            }
        }
    }

    let mut markov = StringMarkovBuilder::new(window);

    for file in files {
        if debug {
            eprintln!("file {}:", &file);
            eprint!("opening...");
        }
        let mut f = match File::open(file.clone()) {
            Result::Ok(v) => v,
            Result::Err(e) => {
                eprintln!("file {} not found: {}", &file, e);
                continue;
            }
        };

        if debug {
            eprint!("reading...");
        }
        let mut contents = String::new();
        match f.read_to_string(&mut contents) {
            Result::Ok(_) => (),
            Result::Err(e) => {
                eprintln!("error reading {}: {}", &file, e);
                continue;
            }
        }

        if debug {
            eprint!("tokenizing...");
        }
        let mut values: Vec<String> = vec![];
        for i in re.find_iter(&contents) {
            values.push(contents[i.start()..i.end()].to_string());
        }

        if debug {
            eprint!("processing...");
        }
        markov.insert_strings(values);
        if debug {
            eprintln!("done");
        }
    }

    //println!("{}", markov.to_string());

    if debug {
        eprintln!("compiling model...");
    }
    let model = markov.compile();

    if debug {
        println!("{}", model.to_string());
    }

    let words = model.generate(limit);

    for word in words {
        print!("{}", word);
    }

    println!("");
}
