extern crate rand;

use std::collections::HashMap;
use std::fmt;

use markov_model::rand::Rng;

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
pub enum Id {
    Id(u32),
    Start, // used for both start and end
}

impl fmt::Display for Id {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Id::Id(id) => write!(f, "|{}", id),
            Id::Start => write!(f, "|_"),
        }
    }
}

// a Vec of these maps a one dimensional space to a series of ids
// the range of each id starts at the previous id's pos (or zero in the case of the first one)
// and ends just before this id's pos
// thus if you choose a random number between zero (inclusive) and the max pos (exclusive)
// you will have selected a random (but weighted) id
pub struct IdPos {
    id: Id,
    pos: u32,
}

pub struct MarkovNode {
    next: Vec<IdPos>,
}

impl MarkovNode {
    pub fn new() -> MarkovNode {
        MarkovNode { next: vec![] }
    }

    pub fn push(&mut self, id: Id, size: u32) {
        let pos = self.max_pos() + size;
        self.next.push(IdPos { id: id, pos: pos });
    }

    fn max_pos(&self) -> u32 {
        match self.next.last() {
            Some(i) => i.pos,
            None => 0,
        }
    }

    fn get_next(&self) -> Id {
        match self.next.len() {
            0 => Id::Start,
            1 => self.next[0].id,
            len => {
                let target = rand::thread_rng().gen_range(0, self.max_pos());
                let mut low = 0;
                let mut high = len - 1;
                while low < high {
                    let test = (low + high) / 2;
                    if target < self.next[test].pos {
                        high = test;
                    } else {
                        low = test + 1;
                    }
                }
                self.next[low].id
            }
        }
    }
}

pub struct MarkovModel {
    nodes: HashMap<Id, MarkovNode>,
}

impl MarkovModel {
    pub fn new(nodes: HashMap<Id, MarkovNode>) -> MarkovModel {
        MarkovModel { nodes: nodes }
    }

    pub fn generate(&self, max_len: u32) -> Vec<u32> {
        let mut node = Id::Start;
        let mut out = vec![];
        let mut i = 0;
        loop {
            node = match self.nodes.get(&node) {
                Some(node) => node.get_next(),
                None => Id::Start,
            };
            match node {
                Id::Id(id) => out.push(id),
                Id::Start => break,
            }
            i += 1;
            if i > max_len {
                break;
            }
        }
        out
    }

    pub fn to_string(&self, id_mapper: &Fn(&Id) -> String) -> String {
        format!(
            "model nodes: [\n{}]\n",
            self.nodes
                .iter()
                .fold(String::new(), |acc, (id, node)| {
                    format!(
                        "{}{}\n",
                        acc,
                        format!(
                            "  {}{}: {{\n{}  }}",
                            id_mapper(id),
                            id,
                            node.next.iter().fold(String::new(), |acc, elem| format!(
                                "{}    {}{}: {},\n",
                                acc,
                                id_mapper(&elem.id),
                                elem.id,
                                elem.pos
                            ))
                        )
                    )
                })
        )
    }
}
