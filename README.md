# Plausible File Generator
This project creates files that are reasonably similar to your existing files should you run it

# Why?
This project was made for the "bad ideas hackathon" at ToorCamp 2018
it has a couple of valid uses, but at the moment is not meant to fill any super important hole 

# How it works.
The python script traverses your home directory and categorizes your text files into a couple of hardcoded categories. It then uses the rust_markov project to create markov models of each of these categories. Finally, plausible file generator uses these markov models to fill up disk space

# How do I use it?
First, run the included build script ```$ ./build.sh``` 
Then, run the python script with N substituted for the number of plausible files you want to generate: ```$ ./plausible.py N```
